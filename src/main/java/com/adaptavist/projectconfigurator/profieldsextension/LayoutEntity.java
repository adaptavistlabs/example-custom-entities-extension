package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCollection;
import com.awnaba.projectconfigurator.extensionpoints.customentities.GlobalCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.IdentifierFactory;
import com.awnaba.projectconfigurator.extensionpoints.customentities.InstanceIndependentIdentifier;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ObjectDescriptor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.Property;
import com.awnaba.projectconfigurator.extensionpoints.customentities.StringProperty;
import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutBuilder;
import com.deiser.jira.profields.api.layout.LayoutService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Profile("pc4j-extensions")
@Component
public class LayoutEntity implements GlobalCustomEntity<Layout> {

	private StringProperty<Layout> nameProperty;
	private StringProperty<Layout> descriptionProperty;
	private LayoutProjectsProperty layoutProjectsProperty;

	private InstanceIndependentIdentifier<Layout> idByName;
	private SortedSectionChildCollection sortedSectionChildCollection;

	private LayoutService layoutService;

	@Inject
	public LayoutEntity(@ComponentImport IdentifierFactory identifierFactory,
						@ComponentImport LayoutService layoutService,
						LayoutProjectsProperty layoutProjectsProperty,
						SortedSectionChildCollection sortedSectionChildCollection) {

		this.layoutService = layoutService;
		this.layoutProjectsProperty = layoutProjectsProperty;

		nameProperty = buildNameProperty();
		idByName = identifierFactory.identifierFromProperties(
				singletonList -> layoutService.getByName((String)singletonList.get(0)), nameProperty);
		this.sortedSectionChildCollection = sortedSectionChildCollection;
		descriptionProperty = buildDescriptionProperty();
	}

	@Override
	public Layout createNew(ObjectDescriptor objectDescriptor) {
		Map<String,Object> properties = objectDescriptor.getProperties();
		LayoutBuilder builder = layoutService.getLayoutBuilder();
		builder.setName((String)properties.get(nameProperty.getPropertyName()));
		builder.setDescription((String)properties.get(descriptionProperty.getPropertyName()));
		return layoutService.create(builder);
	}

	@Override
	public InstanceIndependentIdentifier getCrossInstanceIdentifier() {
		return idByName;
	}

	@Override
	public String getTypeName() {
		return "Layout";
	}

	@Override
	public Collection<Property<Layout,?>> getProperties() {
		return Arrays.asList(nameProperty, descriptionProperty, layoutProjectsProperty);
	}

	@Override
	public Collection<ChildCollection<?, Layout>> getChildCollections() {
		return Collections.singletonList(sortedSectionChildCollection);
	}

	private StringProperty<Layout> buildNameProperty(){

		return new StringProperty<Layout>() {
			@Override
			public String getInternalValue(Layout layout) {
				return layout.getName();
			}

			@Override
			public void setProperty(Layout layout, String s) {
				throw new IllegalStateException("Name of an existing Profield should never be changed");
			}

			@Override
			public String getPropertyName() {
				return "name";
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

	private StringProperty<Layout> buildDescriptionProperty(){

		return new StringProperty<Layout>() {
			@Override
			public String getInternalValue(Layout layout) {
				return layout.getDescription();
			}

			@Override
			public void setProperty(Layout layout, String s) {
				layout.setDescription(s);
				layoutService.update(layout);
			}

			@Override
			public String getPropertyName() {
				return "description";
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}
}
