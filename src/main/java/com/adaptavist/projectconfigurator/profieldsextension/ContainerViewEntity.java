package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCollection;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.IdentifierFactory;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ObjectDescriptor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.PartialIdentifier;
import com.awnaba.projectconfigurator.extensionpoints.customentities.Property;
import com.awnaba.projectconfigurator.extensionpoints.customentities.StringProperty;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.ContainerView;
import com.deiser.jira.profields.api.layout.view.ContainerViewBuilder;
import com.deiser.jira.profields.api.layout.view.SectionView;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Profile("pc4j-extensions")
@Component
public class ContainerViewEntity implements ChildCustomEntity<ContainerViewParent, SectionViewParent> {

    private IdentifierFactory identifierFactory;
    private LayoutService layoutService;
    private ViewStructureUpdater<ContainerViewParent> updater;

    private ChildCollection<FieldViewWithContainer, ContainerViewParent> fieldViewItemChildren;
    private FieldViewItemEntity fieldViewItemEntity;

    private StringProperty<ContainerViewParent> nameProperty;
    private Property<ContainerViewParent, Integer> columnProperty;
    private PartialIdentifier<ContainerViewParent, SectionViewParent> partialIdByName;

    @Inject
    public ContainerViewEntity(@ComponentImport IdentifierFactory identifierFactory,
                               @ComponentImport LayoutService layoutService,
                               FieldViewItemEntity fieldViewItemEntity) {
        this.identifierFactory = identifierFactory;
        this.fieldViewItemEntity = fieldViewItemEntity;
        this.fieldViewItemChildren = buildFieldViewItemChildren();
        this.nameProperty = buildNameProperty();
        this.columnProperty = buildColumnProperty();
        this.partialIdByName = buildPartialIdByName();
        this.layoutService = layoutService;
        this.updater = new ViewStructureUpdater<>(layoutService);
    }

    @Override
    public void delete(SectionViewParent sectionViewParent, ContainerViewParent containerViewParent) {
        updater.remove(
                sectionViewParent.getParent(), containerViewParent,
                (containerVP, sectionViewBuilderList) -> containerVP.findMyBuilderIn(sectionViewBuilderList),
                (containerVP, sectionViewBuilderList) -> sectionViewParent.findMyBuilderIn(sectionViewBuilderList),
                (containerViewBuilder, sectionViewBuilder) -> removeFromColumn(sectionViewBuilder, containerViewBuilder, containerViewParent)
        );
    }

    @Override
    public PartialIdentifier<ContainerViewParent, SectionViewParent> getPartialIdentifier() {
        return partialIdByName;
    }

    @Override
    public ContainerViewParent createNew(
            SectionViewParent sectionViewParent, ObjectDescriptor<ContainerViewParent> objectDescriptor) {
        ContainerViewBuilder builder = layoutService.getContainerViewBuilder();
        String name = (String)objectDescriptor.getProperties().get(nameProperty.getPropertyName());
        builder.setName(name);
        int column = (Integer)objectDescriptor.getProperties().get(columnProperty.getPropertyName());
        updater.add(sectionViewParent.getParent(), builder,
                (containerViewBuilder, sectionViewBuilderList)-> sectionViewParent.findMyBuilderIn(sectionViewBuilderList),
                (itemBuilder, sectionBuilder) -> addInColumn(itemBuilder, sectionBuilder, column));

        return new ContainerViewParent(name, sectionViewParent);
    }

    @Override
    public SectionViewParent getParent(ContainerViewParent containerViewParent) {
        return containerViewParent.getParent();
    }

    @Override
    public String getTypeName() {
        return "Container";
    }

    @Override
    public Collection<Property<ContainerViewParent, ?>> getProperties() {
        return Arrays.asList(nameProperty, columnProperty);
    }

    @Override
    public Collection<ChildCollection<?, ContainerViewParent>> getChildCollections() {
        return Collections.singletonList(fieldViewItemChildren);
    }

    private ChildCollection<FieldViewWithContainer, ContainerViewParent> buildFieldViewItemChildren(){

        return new ChildCollection<FieldViewWithContainer, ContainerViewParent>(){
            @Override
            public ChildCustomEntity<FieldViewWithContainer, ContainerViewParent> getChildCustomEntity() {
                return fieldViewItemEntity;
            }

            @Override
            public List<FieldViewWithContainer> getSubordinates(ContainerViewParent containerViewParent) {
                return getFieldViewItems(containerViewParent);
            }
        };
    }

    private List<FieldViewWithContainer> getFieldViewItems(ContainerViewParent containerViewParent) {
        return containerViewParent.getContainerView().getFieldViews().stream().
                map(fieldView -> new FieldViewWithContainer(fieldView, containerViewParent)).
                collect(Collectors.toList());
    }


    private StringProperty<ContainerViewParent> buildNameProperty(){

        return new StringProperty<ContainerViewParent>() {
            @Override
            public String getInternalValue(ContainerViewParent containerViewParent) {
                return containerViewParent.getContainerView().getName();
            }

            @Override
            public void setProperty(ContainerViewParent sectionViewParent, String s) {
                throw new IllegalStateException("Name of an existing container should never be changed");
            }

            @Override
            public String getPropertyName() {
                return "name";
            }

            @Override
            public boolean isSetInCreation() {
                return true;
            }
        };
    }

    private PartialIdentifier<ContainerViewParent, SectionViewParent> buildPartialIdByName(){
        return identifierFactory.partialIdentifierFromProperties(
                (SectionViewParent sectionViewParent, List<Object> singletonList) -> {
                    String name = (String)singletonList.get(0);
                    return sectionViewParent.getContainerStream().
                            filter(container -> container.getName().equals(name)).
                            map(containerView -> new ContainerViewParent(containerView,sectionViewParent)).
                            findFirst().orElse(null);
                },
                nameProperty);
    }

    private Property<ContainerViewParent, Integer> buildColumnProperty() {
        return new Property<ContainerViewParent, Integer>() {
            @Override
            public Integer getInternalValue(ContainerViewParent containerViewParent) {
                for (int i = 1; i <=3 ; i++){
                    if (isInColumn(containerViewParent.getContainerView(), containerViewParent.getParent().getSectionView(), i)){
                        return i;
                    }
                }
                // This should never happen
                throw new IllegalArgumentException("Container must belong to one of the three columns in its section");
            }

            @Override
            public String getExternalString(ContainerViewParent containerViewParent) {
                return getInternalValue(containerViewParent).toString();
            }

            @Override
            public Integer getInternalValueFrom(String s) {
                return Integer.valueOf(s);
            }

            @Override
            public void setProperty(ContainerViewParent containerViewParent, Integer integer) {
                // This operation would be triggered only if the existing column and the new one are different
                updater.remove(
                        containerViewParent.getParent().getParent(), containerViewParent,
                        (containerVP, sectionViewBuilderList) -> containerVP.findMyBuilderIn(sectionViewBuilderList),
                        (containerVP, sectionViewBuilderList) -> containerVP.getParent().findMyBuilderIn(sectionViewBuilderList),
                        (containerBuilder, sectionViewBuilder) -> moveToColumn(sectionViewBuilder, containerBuilder, containerViewParent, integer)
                );
            }

            @Override
            public String getPropertyName() {
                return "column";
            }

            @Override
            public boolean isSetInCreation() {
                return true;
            }
        };
    }

    private void moveToColumn(SectionViewBuilder parentBuilder, ContainerViewBuilder builder,
                              ContainerViewParent containerViewParent, Integer column) {
        removeFromColumn(parentBuilder, builder, containerViewParent);
        addInColumn(builder,parentBuilder, column);
    }

    private void addInColumn(ContainerViewBuilder itemBuilder, SectionViewBuilder sectionBuilder, int column) {

        switch(column){
            case 1:
                sectionBuilder.addFirstColumn(itemBuilder);
                break;
            case 2:
                sectionBuilder.addSecondColumn(itemBuilder);
                break;
            case 3:
                sectionBuilder.addThirdColumn(itemBuilder);
                break;
            default:
                // This might happen as a result of a broken XML file with wrong column data
                throw new IllegalArgumentException("Sections have only three columns, numbered 1 to 3!");
        }
    }

    private void removeFromColumn(
            SectionViewBuilder sectionViewBuilder, ContainerViewBuilder containerViewBuilder,
            ContainerViewParent containerViewParent) {
        Integer column = columnProperty.getInternalValue(containerViewParent);
        switch(column) {
            case 1:
                sectionViewBuilder.removeFirstColumn(containerViewBuilder);
                break;
            case 2:
                sectionViewBuilder.removeSecondColumn(containerViewBuilder);
                break;
            case 3:
                sectionViewBuilder.removeThirdColumn(containerViewBuilder);
                break;
            default:
                // Being this a private method called from delete() this is NOT going to happen
                throw new IllegalArgumentException("Sections have only three columns, numbered 1 to 3!");
        }
    }

    private boolean isInColumn(ContainerView containerView, SectionView sectionView, int i){
        List<ContainerView> containerViewsInColumn = getContainerViewsInColumn(containerView, sectionView, i);
        String containerName = containerView.getName();
        return containerViewsInColumn.stream().filter(oneContainerView -> oneContainerView.getName().equals(containerName)).
                findFirst().isPresent();
    }

    private List<ContainerView> getContainerViewsInColumn(ContainerView containerView, SectionView sectionView, int i) {

        switch(i){
            case 1: return sectionView.getFirstColumn();
            case 2: return sectionView.getSecondColumn();
            case 3: return sectionView.getThirdColumn();
            default:
                // Being this a private method this is NOT going to happen
                throw new IllegalArgumentException("Sections have only three columns, numbered 1 to 3!");
        }
    }
}
