package com.adaptavist.projectconfigurator.profieldsextension;

import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.ContainerView;
import com.deiser.jira.profields.api.layout.view.SectionView;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;

import java.util.List;
import java.util.stream.Stream;

public class SectionViewParent {

	private LayoutService layoutService;

	private String sectionViewName;
	private Integer parentId;

	public SectionViewParent(SectionView sectionView, Layout parent, LayoutService layoutService) {
		this(sectionView.getName(), parent.getId(), layoutService);
	}

	public SectionViewParent(String sectionViewName, Integer parentId, LayoutService layoutService) {
		this.sectionViewName = sectionViewName;
		this.parentId = parentId;
		this.layoutService = layoutService;
	}

	public SectionView getSectionView() {
		return getParent().getSections().stream().
				filter(section -> section.getName().equals(sectionViewName)).
				findFirst().
				orElseThrow(()-> new IllegalStateException("Unable to find SectionView with name " + sectionViewName));
	}

	public Layout getParent() {
		return layoutService.get(parentId);
	}

	public SectionViewBuilder findMyBuilderIn(List<SectionViewBuilder> sectionViewBuilderList) {
		String parentName = sectionViewName.toUpperCase();
		return sectionViewBuilderList.stream().
				filter(sectionViewBuilder -> sectionViewBuilder.getName().toUpperCase().equals(parentName)).
				findAny().
				orElseThrow(()-> new IllegalStateException("Unable to find SectionViewBuilder with name " + parentName));
	}

	Stream<ContainerView> getContainerStream() {
		SectionView sectionView = getSectionView();
		return Stream.of(
				sectionView.getFirstColumn().stream(),
				sectionView.getSecondColumn().stream(),
				sectionView.getThirdColumn().stream()).flatMap(i -> i);
	}

}
