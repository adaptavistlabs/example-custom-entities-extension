package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.common.ReferenceProcessor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.references.ExportTriggerProperty;
import com.awnaba.projectconfigurator.extensionpoints.customentities.references.ObjectReferenceProcessorFactory;
import com.awnaba.projectconfigurator.operationsapi.ObjectAlias;
import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Profile("pc4j-extensions")
@Component
public class LayoutProjectsProperty implements ExportTriggerProperty<Layout, Project, Project> {

	private LayoutService layoutService;
	private ObjectReferenceProcessorFactory objectReferenceProcessorFactory;

	@Inject
	public LayoutProjectsProperty(@ComponentImport LayoutService layoutService,
								  @ComponentImport ObjectReferenceProcessorFactory objectReferenceProcessorFactory) {
		this.layoutService = layoutService;
		this.objectReferenceProcessorFactory = objectReferenceProcessorFactory;
	}

	@Override
	public String getLinkedEntityName() {
		return ObjectAlias.PROJECT;
	}

	@Override
	public Collection<Layout> getRelatedObjects(Project project) {
		return Collections.singletonList(layoutService.getByProject(project));
	}

	@Override
	public ReferenceProcessor<Project> getReferenceProcessor() {
		return objectReferenceProcessorFactory.getObjectReferenceProcessor(Project.class);
	}

	@Override
	public Project getInternalValue(Layout layout) {
		// Assuming it can have at most one project
		List<Project> projects = layoutService.getProjects(layout);
		return projects.isEmpty() ? null : projects.get(0);
	}

	@Override
	public void setProperty(Layout layout, Project project) {
		layoutService.associateToProject(layout, project.getId());
	}

	@Override
	public String getPropertyName() {
		return "project";
	}

	@Override
	public boolean isSetInCreation() {
		return false;
	}
}
