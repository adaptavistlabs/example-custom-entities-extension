package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.IdentifierFactory;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ObjectDescriptor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.PartialIdentifier;
import com.awnaba.projectconfigurator.extensionpoints.customentities.Property;
import com.awnaba.projectconfigurator.extensionpoints.customentities.StringProperty;
import com.awnaba.projectconfigurator.extensionpoints.customentities.references.ExtendedObjectProperty;
import com.awnaba.projectconfigurator.extensionpoints.customentities.references.ObjectReferenceProcessor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.references.ObjectReferenceProcessorFactory;
import com.deiser.jira.profields.api.field.Field;
import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.FieldViewBuilder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Profile("pc4j-extensions")
@Component
public class FieldViewItemEntity implements ChildCustomEntity<FieldViewWithContainer, ContainerViewParent> {

	private ObjectReferenceProcessorFactory objectReferenceProcessorFactory;
	private IdentifierFactory identifierFactory;

	private LayoutService layoutService;

	private ProfieldEntity profieldEntity;

	private PartialIdentifier<FieldViewWithContainer, ContainerViewParent> partialIdentifier;
	private ExtendedObjectProperty<FieldViewWithContainer, Field> profieldsRefProperty;
	private StringProperty<FieldViewWithContainer> requiredProperty;

	private ViewStructureUpdater<FieldViewWithContainer> updater;

	@Inject
	public FieldViewItemEntity(
			ProfieldEntity profieldEntity,
			@ComponentImport ObjectReferenceProcessorFactory objectReferenceProcessorFactory,
			@ComponentImport IdentifierFactory identifierFactory,
			@ComponentImport LayoutService layoutService) {
		this.objectReferenceProcessorFactory = objectReferenceProcessorFactory;
		this.identifierFactory = identifierFactory;
		this.profieldEntity = profieldEntity;
		this.layoutService = layoutService;

		updater = buildUpdater(layoutService);
		profieldsRefProperty = buildProfieldsRefProperty();
		requiredProperty = buildRequiredProperty();
		partialIdentifier = buildPartialIdentifier();
	}

	@Override
	public FieldViewWithContainer createNew(ContainerViewParent containerViewParent, ObjectDescriptor<FieldViewWithContainer> objectDescriptor) {
		FieldViewBuilder fieldBuilder = layoutService.getFieldViewBuilder();
		fieldBuilder.isRequired(objectDescriptor.getProperties().get(requiredProperty.getPropertyName()).equals("true"));
		Field field = (Field)objectDescriptor.getProperties().get(profieldsRefProperty.getPropertyName());
		fieldBuilder.setField(field);

		updater.add(containerViewParent.getParent().getParent(), fieldBuilder,
				(fvBuilder, sectionViewBuilderList) -> containerViewParent.findMyBuilderIn(sectionViewBuilderList),
				(fvBuilder, containerViewBuilder) -> containerViewBuilder.addFieldView(fvBuilder));
		return new FieldViewWithContainer(field, containerViewParent);
	}

	@Override
	public void delete(ContainerViewParent containerViewParent, FieldViewWithContainer fieldViewWithContainer) {
		updater.remove(
		        getLayoutFor(fieldViewWithContainer), fieldViewWithContainer,
                (fieldViewWC, sectionViewBuilders) -> fieldViewWC.findMyBuilderIn(sectionViewBuilders),
                (fieldViewWC, sectionViewBuilders) -> containerViewParent.findMyBuilderIn(sectionViewBuilders),
				(fieldViewBuilder, containerViewBuilder) -> containerViewBuilder.removeFieldView(fieldViewBuilder)
        );
	}

    @Override
    public ContainerViewParent getParent(FieldViewWithContainer fieldViewWithContainer) {
        return fieldViewWithContainer.getContainerViewParent();
    }

    @Override
    public String getTypeName() {
        return "Field view item";
    }

    @Override
    public Collection<Property<FieldViewWithContainer, ?>> getProperties() {
        return Arrays.asList(profieldsRefProperty, requiredProperty);
    }

	@Override
	public PartialIdentifier<FieldViewWithContainer, ContainerViewParent> getPartialIdentifier() {
		return partialIdentifier;
	}

	private PartialIdentifier<FieldViewWithContainer, ContainerViewParent> buildPartialIdentifier() {
		return identifierFactory.partialIdentifierFromProperties(this::findByField, profieldsRefProperty);
	}

    private ViewStructureUpdater<FieldViewWithContainer> buildUpdater(LayoutService layoutService) {
        return new ViewStructureUpdater<>(
                layoutService
        );
    }

	private FieldViewWithContainer findByField(ContainerViewParent container, List<Object> singletonList){
		Integer fieldId = ((Field) (singletonList.get(0))).getId();
		return container.getContainerView().getFieldViews().stream().
				filter(fv -> fv.getField().getId().equals(fieldId)).
				map(fv -> new FieldViewWithContainer(fv, container)).
				findFirst().orElse(null);
	}

	private StringProperty<FieldViewWithContainer> buildRequiredProperty(){
		return new StringProperty<FieldViewWithContainer>(){
			@Override
			public String getInternalValue(FieldViewWithContainer fieldViewWithContainer) {
				return fieldViewWithContainer.getFieldView().isRequired() ? "true" : "false";
			}

			@Override
			public void setProperty(FieldViewWithContainer fieldViewWithContainer, String s) {
				boolean isRequired = "true".equals(s);
				updater.<FieldViewBuilder>update(getLayoutFor(fieldViewWithContainer), fieldViewWithContainer,
						(fvWithContainer, sectionViewBuilderList)-> fvWithContainer.findMyBuilderIn(sectionViewBuilderList),
						fvBuilder -> fvBuilder.isRequired(isRequired));
			}

			@Override
			public String getPropertyName() {
				return "required";
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

	private ExtendedObjectProperty<FieldViewWithContainer, Field> buildProfieldsRefProperty(){
		return new ExtendedObjectProperty<FieldViewWithContainer, Field>(){
			@Override
			public ObjectReferenceProcessor<Field> getReferenceProcessor() {
				// This is an ObjectReferenceProcessor to one of the entities defined in this app
				return objectReferenceProcessorFactory.getObjectReferenceProcessor(profieldEntity);
			}

			@Override
			public Field getInternalValue(FieldViewWithContainer fieldViewWithContainer) {
				return fieldViewWithContainer.getFieldView().getField();
			}

			@Override
			public void setProperty(FieldViewWithContainer fieldViewWithContainer, Field field) {
				throw new IllegalStateException("The field of an existing FieldView should never be modified");
			}

			@Override
			public String getPropertyName() {
				return "Profield";
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

    private Layout getLayoutFor(FieldViewWithContainer fieldViewWithContainer) {
        return fieldViewWithContainer.getContainerViewParent().getParent().getParent();
    }

}
