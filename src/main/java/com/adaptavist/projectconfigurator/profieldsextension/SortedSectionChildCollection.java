package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.SortedChildCollection;
import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Profile("pc4j-extensions")
@Component
public class SortedSectionChildCollection implements SortedChildCollection<SectionViewParent, Layout> {

    private SectionViewEntity sectionViewEntity;
    private ViewStructureUpdater<SectionViewParent> updater;

    private LayoutService layoutService;

    @Inject
    public SortedSectionChildCollection(
                        @ComponentImport LayoutService layoutService,
                        SectionViewEntity sectionViewEntity) {

        this.sectionViewEntity = sectionViewEntity;
        this.layoutService = layoutService;
        updater = new ViewStructureUpdater<>(layoutService);
    }

    @Override
    public ChildCustomEntity<SectionViewParent, Layout> getChildCustomEntity() {
        return sectionViewEntity;
    }

    @Override
    public List<SectionViewParent> getSubordinates(Layout layout) {
        return layout.getSections().stream().
                map(section -> new SectionViewParent(section, layout, layoutService)).
                collect(Collectors.toList());
    }

    @Override
    public void sort(List<String> inputList, Layout layout) {
        updater.update(layout, null,
                (ignoredItem, builderList) -> builderList,
                builderList -> sortAs(builderList, inputList));
    }

    private void sortAs(List<SectionViewBuilder> builderList, List<String> inputList) {
        builderList.sort(Comparator.comparingInt(o -> inputList.indexOf(o.getName())));
    }
}
