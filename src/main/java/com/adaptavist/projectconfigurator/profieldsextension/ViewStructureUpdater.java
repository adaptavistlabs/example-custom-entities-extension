package com.adaptavist.projectconfigurator.profieldsextension;

import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class ViewStructureUpdater<T> {

    private LayoutService layoutService;

    public ViewStructureUpdater(LayoutService layoutService) {
        this.layoutService = layoutService;
    }

    <B> void update(Layout root, T item, BiFunction<T, List<SectionViewBuilder>, B> referenceBuilderFinder, Consumer<B> changeImplementation) {
        List<SectionViewBuilder> builderList = layoutService.getSectionViewBuildersFrom(root);
        B referenceBuilder = referenceBuilderFinder.apply(item, builderList);
        changeImplementation.accept(referenceBuilder);
        root.setSectionViewBuilders(builderList);
        layoutService.update(root);
    }

    <B, PB> void add(Layout root, B itemBuilder, BiFunction<B, List<SectionViewBuilder>, PB> parentBuilderFinder, BiConsumer<B,PB> addAction){
        List<SectionViewBuilder> builderList = layoutService.getSectionViewBuildersFrom(root);
        PB parentBuilder = parentBuilderFinder.apply(itemBuilder, builderList);
        addAction.accept(itemBuilder, parentBuilder);
        root.setSectionViewBuilders(builderList);
        layoutService.update(root);
    }

    <T, B, PB> void remove(Layout root, T item,
                           BiFunction<T, List<SectionViewBuilder>, B> itemBuilderFinder,
                           BiFunction<T, List<SectionViewBuilder>, PB> parentBuilderFinder,
                           BiConsumer<B, PB> removeAction) {
        List<SectionViewBuilder> builderList = layoutService.getSectionViewBuildersFrom(root);
        PB parentBuilder = parentBuilderFinder.apply(item, builderList);
        B itemBuilder = itemBuilderFinder.apply(item, builderList);
        removeAction.accept(itemBuilder, parentBuilder);
        root.setSectionViewBuilders(builderList);
        layoutService.update(root);
    }

}
