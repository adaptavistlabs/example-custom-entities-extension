package com.adaptavist.projectconfigurator.profieldsextension;

import com.deiser.jira.profields.api.field.Field;
import com.deiser.jira.profields.api.layout.view.ContainerViewBuilder;
import com.deiser.jira.profields.api.layout.view.FieldView;
import com.deiser.jira.profields.api.layout.view.FieldViewBuilder;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;

import java.util.List;

public class FieldViewWithContainer {

	private Integer fieldId;
	private ContainerViewParent containerViewParent;

	public FieldViewWithContainer(FieldView fieldView, ContainerViewParent containerViewParent) {
		this(fieldView.getField(), containerViewParent);
	}

	public FieldViewWithContainer(Field field, ContainerViewParent containerViewParent) {
		this.fieldId = field.getId();
		this.containerViewParent = containerViewParent;
	}

	public FieldView getFieldView() {
		return containerViewParent.getContainerView().getFieldViews().stream().
				filter(fieldView -> fieldView.getField().getId().equals(fieldId)).
				findFirst().
				orElseThrow(()-> new IllegalStateException("Unable to find FieldView with field id " + fieldId));
	}

	public ContainerViewParent getContainerViewParent() {
		return containerViewParent;
	}

	public FieldViewBuilder findMyBuilderIn(List<SectionViewBuilder> sectionViewBuilderList) {
		ContainerViewBuilder parentBuilder = containerViewParent.findMyBuilderIn(sectionViewBuilderList);
		return parentBuilder.getFieldViewList().stream().
				filter(fieldViewBuilder -> fieldViewBuilder.getField().getId().equals(fieldId)).
				findFirst().
				orElseThrow(()-> new IllegalStateException("Unable to find FieldViewBuilder with field id " + fieldId));
	}
}
