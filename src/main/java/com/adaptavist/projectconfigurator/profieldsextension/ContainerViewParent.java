package com.adaptavist.projectconfigurator.profieldsextension;

import com.deiser.jira.profields.api.layout.view.ContainerView;
import com.deiser.jira.profields.api.layout.view.ContainerViewBuilder;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;

import java.util.List;
import java.util.stream.Stream;

public class ContainerViewParent {

	private String containerViewName;
	private SectionViewParent parent;

	public ContainerViewParent(ContainerView containerView, SectionViewParent parent) {
		this(containerView.getName(),parent);
	}

	public ContainerViewParent(String containerViewName, SectionViewParent parent) {
		this.containerViewName = containerViewName;
		this.parent = parent;
	}

	public ContainerView getContainerView() {
		return parent.getContainerStream().
				filter(containerView -> containerView.getName().equals(containerViewName)).
				findAny().
				orElseThrow(()-> new IllegalStateException("Unable to find ContainerView with name " + containerViewName));
	}

	public SectionViewParent getParent() {
		return parent;
	}

	public ContainerViewBuilder findMyBuilderIn(List<SectionViewBuilder> sectionViewBuilderList) {
		SectionViewBuilder parentBuilder = getParent().findMyBuilderIn(sectionViewBuilderList);
		return getContainerBuilderStreamFor(parentBuilder).
				filter(containerViewBuilder -> containerViewBuilder.getName().equals(containerViewName)).
				findFirst().
				orElseThrow(()-> new IllegalStateException("Unable to find ContainerViewBuilder with name " + containerViewName));
	}

	private Stream<ContainerViewBuilder> getContainerBuilderStreamFor(SectionViewBuilder sectionViewBuilder){
		return Stream.of(
				sectionViewBuilder.getFirstColumn().stream(),
				sectionViewBuilder.getSecondColumn().stream(),
				sectionViewBuilder.getThirdColumn().stream()).flatMap(i -> i);
	}
}
