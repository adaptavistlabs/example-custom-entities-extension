package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.GlobalCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.IdentifierFactory;
import com.awnaba.projectconfigurator.extensionpoints.customentities.InstanceIndependentIdentifier;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ObjectDescriptor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.Property;
import com.awnaba.projectconfigurator.extensionpoints.customentities.StringProperty;
import com.deiser.jira.profields.api.field.Field;
import com.deiser.jira.profields.api.field.FieldBuilder;
import com.deiser.jira.profields.api.field.FieldService;
import com.deiser.jira.profields.api.field.FieldType;
import com.deiser.jira.profields.api.field.date.DateFieldBuilder;
import com.deiser.jira.profields.api.field.number.NumberFieldBuilder;
import com.deiser.jira.profields.api.field.text.TextFieldBuilder;
import com.deiser.jira.profields.api.field.textmultiple.TextMultipleFieldBuilder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Profile("pc4j-extensions")
@Component
public class ProfieldEntity implements GlobalCustomEntity<Field> {

	private static final String FIELD_TYPE_PROPERTY_NAME = "type";
	private static final String DESCRIPTION_PROPERTY_NAME = "description";
	private static final String NAME_PROPERTY_NAME = "name";
	private IdentifierFactory identifierFactory;
	private FieldService fieldService;

	private StringProperty<Field> nameProperty;
	private StringProperty<Field> descriptionProperty;
	private Property<Field, FieldType> fieldTypeProperty;

	private InstanceIndependentIdentifier<Field> idByNameAndType;

	@Inject
	public ProfieldEntity(@ComponentImport IdentifierFactory identifierFactory,
						  @ComponentImport FieldService fieldService) {
		this.identifierFactory = identifierFactory;
		this.fieldService = fieldService;

		nameProperty = buildNameProperty();
		descriptionProperty = buildDescriptionProperty();
		fieldTypeProperty = buildFieldTypeProperty();
		idByNameAndType = buildNameTypeIdentifier();
	}

	@Override
	public Field createNew(ObjectDescriptor<Field> objectDescriptor) {
		FieldType newFieldType =
				(FieldType) objectDescriptor.getProperties().get(FIELD_TYPE_PROPERTY_NAME);
		return buildField(newFieldType, objectDescriptor);
	}

	private Field buildField(FieldType fieldType, ObjectDescriptor<Field> objectDescriptor) {
		switch(fieldType) {
			case DATE: {
				DateFieldBuilder builder = fieldService.getDateFieldBuilder();
				applyCommonProcesses(builder, objectDescriptor);
				return fieldService.create(builder);
			}
			case TEXT: {
				TextFieldBuilder builder = fieldService.getTextFieldBuilder();
				applyCommonProcesses(builder, objectDescriptor);
				return fieldService.create(builder);
			}
			case TEXT_MULTIPLE: {
				TextMultipleFieldBuilder builder = fieldService.getTextMultipleFieldBuilder();
				applyCommonProcesses(builder, objectDescriptor);
				return fieldService.create(builder);
			}
			case NUMBER: {
				NumberFieldBuilder builder = fieldService.getNumberFieldBuilder();
				applyCommonProcesses(builder, objectDescriptor);
				return fieldService.create(builder);
			}
			default:
				throw new IllegalArgumentException("Field type " + fieldType +
						" not supported by this example");
		}
	}

	private void applyCommonProcesses(FieldBuilder builder, ObjectDescriptor<Field> objectDescriptor) {
		builder.setName((String)objectDescriptor.getProperties().get(NAME_PROPERTY_NAME));
		builder.setDescription((String)objectDescriptor.getProperties().get(DESCRIPTION_PROPERTY_NAME));
	}

	@Override
	public InstanceIndependentIdentifier<Field> getCrossInstanceIdentifier() {
		return idByNameAndType;
	}

	@Override
	public String getTypeName() {
		return "Profield";
	}

	@Override
	public Collection<Property<Field, ?>> getProperties() {
		return Arrays.asList(nameProperty, descriptionProperty, fieldTypeProperty);
	}

	private InstanceIndependentIdentifier<Field> buildNameTypeIdentifier(){
		return identifierFactory.identifierFromProperties(
				list -> findFieldByNameAndType(list.get(0).toString(), (FieldType)list.get(1)),
		nameProperty, fieldTypeProperty);
	}

	private Field findFieldByNameAndType(String name, FieldType type){
		List<Field> fields = fieldService.get(name);
		fields.removeIf(field -> !field.getType().equals(type));
		return fields.isEmpty() ? null : fields.get(0);
	}

	private StringProperty<Field> buildNameProperty(){

		return new StringProperty<Field>() {
			@Override
			public String getInternalValue(Field field) {
				return field.getName();
			}

			@Override
			public void setProperty(Field field, String s) {
				throw new IllegalStateException("Name of an existing Profield should never be changed");
			}

			@Override
			public String getPropertyName() {
				return NAME_PROPERTY_NAME;
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

	private StringProperty<Field> buildDescriptionProperty(){

		return new StringProperty<Field>() {
			@Override
			public String getInternalValue(Field field) {
				return field.getDescription();
			}

			@Override
			public void setProperty(Field field, String newDescription) {
				field.setDescription(newDescription);
				fieldService.update(field);
			}

			@Override
			public String getPropertyName() {
				return DESCRIPTION_PROPERTY_NAME;
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

	private Property<Field, FieldType> buildFieldTypeProperty(){
		return new Property<Field, FieldType>() {
			@Override
			public FieldType getInternalValue(Field field) {
				return field.getType();
			}

			@Override
			public String getExternalString(Field field) {
				return getInternalValue(field).getName();
			}

			@Override
			public FieldType getInternalValueFrom(String s) {
				return FieldType.getFieldTypeByKey(s);
			}

			@Override
			public void setProperty(Field field, FieldType fieldType) {
				throw new IllegalStateException("Type of an existing Profield should never be changed");
			}

			@Override
			public String getPropertyName() {
				return FIELD_TYPE_PROPERTY_NAME;
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}
}
