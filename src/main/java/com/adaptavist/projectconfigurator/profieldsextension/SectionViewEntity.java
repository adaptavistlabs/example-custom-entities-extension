package com.adaptavist.projectconfigurator.profieldsextension;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCollection;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ChildCustomEntity;
import com.awnaba.projectconfigurator.extensionpoints.customentities.IdentifierFactory;
import com.awnaba.projectconfigurator.extensionpoints.customentities.ObjectDescriptor;
import com.awnaba.projectconfigurator.extensionpoints.customentities.PartialIdentifier;
import com.awnaba.projectconfigurator.extensionpoints.customentities.Property;
import com.awnaba.projectconfigurator.extensionpoints.customentities.StringProperty;
import com.deiser.jira.profields.api.layout.Layout;
import com.deiser.jira.profields.api.layout.LayoutService;
import com.deiser.jira.profields.api.layout.view.SectionViewBuilder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Profile("pc4j-extensions")
@Component
public class SectionViewEntity implements ChildCustomEntity<SectionViewParent, Layout> {

	private LayoutService layoutService;
	private IdentifierFactory identifierFactory;

	private StringProperty<SectionViewParent> nameProperty;
	private PartialIdentifier<SectionViewParent, Layout> partialIdByName;
	private ContainerViewEntity containerViewEntity;

	private ChildCollection<ContainerViewParent, SectionViewParent> containerViewChildren;
	private ViewStructureUpdater<SectionViewParent> updater;

	@Inject
	public SectionViewEntity(@ComponentImport LayoutService layoutService,
							 @ComponentImport IdentifierFactory identifierFactory,
                             ContainerViewEntity containerViewEntity) {
		this.layoutService = layoutService;
		this.identifierFactory = identifierFactory;
		this.containerViewEntity = containerViewEntity;
		updater = buildUpdater();
		nameProperty = buildNameProperty();
		partialIdByName = buildPartialIdByName();
		containerViewChildren = buildContainerViewChildren();
	}

	@Override
	public void delete(Layout layout, SectionViewParent sectionViewParent) {
		updater.remove(
		        layout, sectionViewParent,
                (sectionViewP, sectionViewBuilderList) -> sectionViewP.findMyBuilderIn(sectionViewBuilderList),
                (sectionViewP, sectionViewBuilderList) -> sectionViewBuilderList,
                (sectionViewBuilder, sectionViewBuilderList) -> sectionViewBuilderList.remove(sectionViewBuilder)
        );
	}

	@Override
	public PartialIdentifier<SectionViewParent, Layout> getPartialIdentifier() {
		return partialIdByName;
	}

	@Override
	public SectionViewParent createNew(Layout layout, ObjectDescriptor<SectionViewParent> objectDescriptor) {
		SectionViewBuilder sectionBuilder = layoutService.getSectionViewBuilder();
		String newName = (String)objectDescriptor.getProperties().get(nameProperty.getPropertyName());
		sectionBuilder.setName(newName);

		updater.add(
		        layout, sectionBuilder,
                (itemBuilder, sectionViewBuilderList) -> sectionViewBuilderList,
                (itemBuilder, sectionViewBuilderList) -> sectionViewBuilderList.add(sectionBuilder)
        );
		return new SectionViewParent(newName, layout.getId(), layoutService);
	}

	@Override
	public Layout getParent(SectionViewParent sectionViewParent) {
		return sectionViewParent.getParent();
	}

	@Override
	public String getTypeName() {
		return "Section";
	}

	@Override
	public Collection<Property<SectionViewParent, ?>> getProperties() {
		return Collections.singletonList(nameProperty);
	}

	@Override
    public Collection<ChildCollection<?, SectionViewParent>> getChildCollections() {
        return Collections.singletonList(containerViewChildren);
    }

    private ViewStructureUpdater<SectionViewParent> buildUpdater() {
        return new ViewStructureUpdater<>(layoutService);
    }

    private ChildCollection<ContainerViewParent, SectionViewParent> buildContainerViewChildren(){

		return new ChildCollection<ContainerViewParent, SectionViewParent>(){
			@Override
			public ChildCustomEntity<ContainerViewParent, SectionViewParent> getChildCustomEntity() {
				return containerViewEntity;
			}

			@Override
			public List<ContainerViewParent> getSubordinates(SectionViewParent sectionViewParent) {
			    return sectionViewParent.getContainerStream().
                        map(containerView -> new ContainerViewParent(containerView, sectionViewParent)).
                        collect(Collectors.toList());
			}
		};
	}

	private StringProperty<SectionViewParent> buildNameProperty(){

		return new StringProperty<SectionViewParent>() {
			@Override
			public String getInternalValue(SectionViewParent sectionViewParent) {
				return sectionViewParent.getSectionView().getName();
			}

			@Override
			public void setProperty(SectionViewParent sectionViewParent, String s) {
				throw new IllegalStateException("Name of an existing section should never be changed");
			}

			@Override
			public String getPropertyName() {
				return "name";
			}

			@Override
			public boolean isSetInCreation() {
				return true;
			}
		};
	}

	private PartialIdentifier<SectionViewParent, Layout> buildPartialIdByName(){
		return identifierFactory.partialIdentifierFromProperties(
				(Layout layout, List<Object> singletonList) -> {
					String name = (String)singletonList.get(0);
					return layout.getSections().stream().
							filter(section -> section.getName().equals(name)).
							map(sectionView -> new SectionViewParent(sectionView, layout, layoutService)).
							findFirst().orElse(null);
				},
				nameProperty);
	}
}
